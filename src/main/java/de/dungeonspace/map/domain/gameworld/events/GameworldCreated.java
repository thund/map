package de.dungeonspace.map.domain.gameworld.events;

import de.dungeonspace.map.domain.gameworld.Gameworld;

public record GameworldCreated(
    Gameworld gameworld
) {

}
