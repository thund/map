package de.dungeonspace.map.domain.gameworld.events;

import de.dungeonspace.map.domain.gameworld.GameworldStatus;
import java.util.UUID;

public record GameworldStatusChanged(
    UUID id,
    GameworldStatus status
) {

}
