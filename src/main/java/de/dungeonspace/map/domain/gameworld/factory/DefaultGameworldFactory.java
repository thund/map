package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Area;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.planet.Resource;
import de.dungeonspace.map.domain.planet.ResourceType;
import de.dungeonspace.map.domain.primitive.Coordinates;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class DefaultGameworldFactory implements GameworldFactory {
  private final int mapSize;
  private static final Random random = new Random();

  private static final Map<ResourceType, Double> RESOURCE_QUOTA = Map.of(
      ResourceType.COAL, 0.8,
      ResourceType.IRON, 0.5,
      ResourceType.GEM, 0.3,
      ResourceType.GOLD, 0.2,
      ResourceType.PLATIN, 0.1
  );
  private static final Map<ResourceType, Set<Area>> RESOURCE_LOCATION = Map.of(
      ResourceType.COAL, Set.of(Area.OUTER, Area.BORDER),
      ResourceType.IRON, Set.of(Area.MID),
      ResourceType.GEM, Set.of(Area.MID),
      ResourceType.GOLD, Set.of(Area.INNER),
      ResourceType.PLATIN, Set.of(Area.INNER)
  );

  private static final Integer DEFAULT_RESOURCE_AMOUNT = 10_000;

  public DefaultGameworldFactory(int playerAmount) {
    this.mapSize = calculateMapSize(playerAmount);
  }

  @Override
  public Gameworld create() {
    var grid = MapGrid.create(mapSize);

    deleteRandomPlanets(grid);
    distributeResources(grid);

    return Gameworld.create(grid);
  }

  private void distributeResources(MapGrid mapGrid) {
    var resources = ResourceType.values();
    for(ResourceType rt : resources) {
      var quota = RESOURCE_QUOTA.get(rt);
      var locations = RESOURCE_LOCATION.get(rt);

      if(quota == null || locations == null) {
        continue;
      }

      var resource = new Resource(rt, DEFAULT_RESOURCE_AMOUNT);
      var planets = mapGrid.getPlanets(locations.toArray(Area[]::new));
      var planetsWithoutResources = planets.entrySet()
          .stream()
          .filter(entry -> !entry.getValue().hasResource())
          .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
      var keys = planetsWithoutResources.keySet().stream().limit(
          (long) (planetsWithoutResources.size() * quota)).collect(Collectors.toList());
      Collections.shuffle(keys);

      for(var key : keys) {
        planetsWithoutResources.get(key).createResource(resource);
      }
      mapGrid.patchPlanets(planetsWithoutResources);
    }
  }

  private void deleteRandomPlanets(MapGrid grid) {
    // We just delete planets from the outer and mid area
    var planets = grid.getPlanets(Area.OUTER, Area.MID);
    var amount = calculateDeleteAmount(grid);
    List<Coordinates> keys = new ArrayList<>(planets.keySet());
    Collections.shuffle(keys);

    var deletedKeys = new ArrayList<>();
    for (int i = 0; deletedKeys.size() < amount && i < keys.size(); i++) {
      var toDelete = keys.get(i);

      // We don't want to isolate planets on the grid.
      if(grid.getDirectNeighbours(toDelete).keySet().size() <= 1) {
        continue;
      }

      grid.destroyPlanet(toDelete);
      deletedKeys.add(toDelete);
    }
  }

  private int calculateDeleteAmount(MapGrid grid) {
    return grid.getPlanets().size() / 10 * random.nextInt(1, 3);
  }

  private int calculateMapSize(int playerAmount) {
    if(playerAmount < 10)
      return 15;
    if(playerAmount < 20)
      return 20;
    return 35;
  }

}
