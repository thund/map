package de.dungeonspace.map.domain.gameworld.factory;

import de.dungeonspace.map.domain.gameworld.Gameworld;
import java.util.Map;

public interface GameworldFactory {
  Gameworld create();
}
