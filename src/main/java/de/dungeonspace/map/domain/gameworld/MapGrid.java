package de.dungeonspace.map.domain.gameworld;

import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.primitive.Coordinates;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Embeddable;
import jakarta.persistence.OneToMany;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MapGrid {
  private static final int MIN_SIZE = 5;

  @Getter
  private int size;

  // Planet does not make any sense without a gameworld, therefore we can cascade here
  @OneToMany(cascade = CascadeType.ALL)
  private Map<Coordinates, Planet> planets = new HashMap<>();

  /**
   * Creates a new map grid with the given size.
   * @param mapSize size/width of grid
   * @return new map grid
   */
  public static MapGrid create(int mapSize) {
    if(mapSize < MIN_SIZE) {
      throw new IllegalArgumentException("Map size must be at least " + MIN_SIZE);
    }

    var mapGrid = new MapGrid();
    mapGrid.size = mapSize;
    mapGrid.planets = mapGrid.createNew(mapSize);
    return mapGrid;
  }

  /**
   * Destroys a planet at the given coordinates.
   * @param coordinates The coordinates of the planet to destroy.
   */
  public void destroyPlanet(Coordinates coordinates) {
    planets.remove(coordinates);
  }

  /**
   * Patches the given planets in the map grid. Cannot be used to add new planets, only existing
   * planets will be patched.
   * @param planets the planets to patch
   */
  public void patchPlanets(Map<Coordinates, Planet> planets) {
    for (var entry : planets.entrySet()) {
      if(this.planets.containsKey(entry.getKey())) {
        this.planets.put(entry.getKey(), entry.getValue());
      }
    }
  }

  /**
   * Retrieves the whole grid.
   * @return grid as an unmodifiable map
   */
  public Map<Coordinates, Planet> getPlanets() {
    return Map.copyOf(planets);
  }

  /**
   * Retrieves a subset of the grid from the given areas
   * @param area the area to retrieve
   * @return a subset of the grid
   */
  public Map<Coordinates, Planet> getPlanets(Area... area) {
    return planets.entrySet()
        .stream()
        .filter(entry -> Arrays.stream(area).anyMatch(a -> a.equals(areaOf(entry.getKey()))))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  public Map<Coordinates, Planet> getDirectNeighbours(Coordinates coordinates) {
    return this.planets.entrySet()
        .stream()
        .filter(entry -> entry.getKey().isAdjacent(coordinates))
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  public Coordinates coordinatesOf(Planet planet) {
    return this.planets.entrySet()
        .stream()
        .filter(entry -> entry.getValue().equals(planet))
        .map(Map.Entry::getKey)
        .findFirst()
        .orElseThrow();
  }

  public Area areaOf(Coordinates coordinates) {
    var size = this.size - 1;
    var innerRadius = size / 3;
    var midRadius = innerRadius / 2;
    var x = coordinates.getX();
    var y = coordinates.getY();

    if(x == 0 || y == 0 || x == size || y == size) {
      return Area.BORDER;
    }
    if (x > innerRadius && x < size - innerRadius && y > innerRadius && y < size - innerRadius) {
      return Area.INNER;
    }
    if (x > midRadius && x < size - midRadius && y > midRadius && y < size - midRadius) {
      return Area.MID;
    }

    return Area.OUTER;
  }

  private Map<Coordinates, Planet> createNew(int size) {
    return IntStream.range(0, size)
        .mapToObj(x -> IntStream.range(0, size)
            .mapToObj(y -> new Coordinates(x, y)))
        .flatMap(x -> x)
        .collect(Collectors.toMap(c -> c, c -> Planet.create()));
  }
}
