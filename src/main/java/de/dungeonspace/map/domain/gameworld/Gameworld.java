package de.dungeonspace.map.domain.gameworld;

import de.dungeonspace.map.domain.gameworld.events.GameworldCreated;
import de.dungeonspace.map.domain.gameworld.events.GameworldStatusChanged;
import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.primitive.Coordinates;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Type;
import org.springframework.data.domain.AbstractAggregateRoot;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Gameworld extends AbstractAggregateRoot<Gameworld> {
  @Id
  private UUID id = UUID.randomUUID();

  @NotNull
  @Enumerated(EnumType.STRING)
  private GameworldStatus status = null;

  @Embedded
  private MapGrid mapGrid;

  public static Gameworld create(MapGrid mapGrid) {
    var gw = new Gameworld(mapGrid);
    gw.registerEvent(new GameworldCreated(gw));
    return gw;
  }

  private Gameworld(MapGrid mapGrid) {
    this.mapGrid = mapGrid;
  }

  public void changeStatus(GameworldStatus status) {
    this.status = status;
    this.registerEvent(new GameworldStatusChanged(this.id, this.status));
  }
}
