package de.dungeonspace.map.domain.gameworld;

public enum GameworldStatus {
  ACTIVE, INACTIVE
}
