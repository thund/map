package de.dungeonspace.map.domain.gameworld;

/**
 * Represents a section of the Gameworld.
 */
public enum Area {
  INNER(3), MID(2), OUTER(1), BORDER(1);

  private final int movementDifficulty;

  Area(int movementDifficulty) {
    this.movementDifficulty = movementDifficulty;
  }

  /**
   * Movement difficulty of this area.
   * @return movement difficulty
   */
  public int getMovementDifficulty() {
    return movementDifficulty;
  }
}
