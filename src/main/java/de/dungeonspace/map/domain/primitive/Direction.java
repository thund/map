package de.dungeonspace.map.domain.primitive;

public enum Direction {
  NORTH, EAST, SOUTH, WEST
}
