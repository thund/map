package de.dungeonspace.map.domain.primitive;

import jakarta.persistence.Embeddable;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Primitive for coordinates.
 * We're using a 2D coordinate system.
 */
@Embeddable
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class Coordinates {
  private int x;
  private int y;

  public boolean isAdjacent(Coordinates other) {
    return other.x == x && Math.abs(other.y - y) == 1
        || other.y == y && Math.abs(other.x - x) == 1;
  }

  public Direction getRelativeDirectionTo(Coordinates other) {
    if(other.x == x && other.y > y) {
      return Direction.NORTH;
    } else if(other.x == x && other.y < y) {
      return Direction.SOUTH;
    } else if(other.y == y && other.x > x) {
      return Direction.EAST;
    } else if(other.y == y && other.x < x) {
      return Direction.WEST;
    }

    throw new RuntimeException("Coordinates are not adjacent");
  }
}
