package de.dungeonspace.map.domain.planet;

import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.planet.event.ResourceMined;
import de.dungeonspace.map.domain.planet.exception.NoResourceAvailableException;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import java.util.UUID;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.AbstractAggregateRoot;
import org.springframework.lang.Nullable;

@Entity
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Getter
public class Planet extends AbstractAggregateRoot<Planet> {

  @Id
  private UUID id = UUID.randomUUID();
  @Embedded
  @Nullable
  @Enumerated(EnumType.STRING)
  private Resource resource;

  public static Planet create() {
    return new Planet();
  }

  /**
   * Mines a given amount of the resource. If the amount is not available, the maximum possible amount is mined
   * and if there is no resource left, an exception is thrown.
   * @throws RuntimeException if there is no resource left
   * @param amount the amount to mine
   */
  public void mine(int amount) {
    if(!hasResource()) {
      throw new NoResourceAvailableException("Planet '%s' does not have any resource".formatted(this.id));
    }
    assert resource != null;

    var currentAmount = this.resource.getCurrentAmount();
    if(currentAmount == 0) {
      throw new NoResourceAvailableException("Planet '%s' does not have any resource left".formatted(this.id));
    }

    var diff = currentAmount - amount;
    var newAmount = Math.max(diff, 0);
    this.resource.setCurrentAmount(newAmount);

    var minedAmount = Math.min(amount, newAmount);
    this.registerEvent(new ResourceMined(this.id, minedAmount, resource));
  }

  /**
   * Places a new resource on the planet.
   * @throws RuntimeException if there is already a resource on the planet
   * @param resource
   */
  public void createResource(Resource resource) {
    if(this.hasResource()) {
      throw new RuntimeException("Resource already exists");
    }
    this.resource = resource;
  }

  /**
   * Checks if there is a resource on the planet.
   * @return true if there is a resource, false otherwise
   */
  public boolean hasResource() {
    return resource != null;
  }
}
