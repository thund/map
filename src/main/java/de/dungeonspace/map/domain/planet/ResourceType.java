package de.dungeonspace.map.domain.planet;

public enum ResourceType {
  COAL,
  IRON,
  GEM,
  GOLD,
  PLATIN
}
