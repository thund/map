package de.dungeonspace.map.domain.planet.event;

import de.dungeonspace.map.domain.planet.Resource;
import java.util.UUID;

public record ResourceMined(
    UUID planet,
    int minedAmount,
    Resource resource
) {

}
