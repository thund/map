package de.dungeonspace.map.domain.planet.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NoResourceAvailableException extends RuntimeException {
  public NoResourceAvailableException(String message) {
    super(message);
  }
}
