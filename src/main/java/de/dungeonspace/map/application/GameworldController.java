package de.dungeonspace.map.application;

import de.dungeonspace.map.application.commands.CreateNewGameworld;
import de.dungeonspace.map.application.commands.dto.CreateGameworldDto;
import de.dungeonspace.map.application.commands.dto.CreateGameworldResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/gameworlds")
@RequiredArgsConstructor
public class GameworldController {
  private final CreateNewGameworld createNewGameworld;

  @PostMapping(produces = "application/json", consumes = "application/json")
  public ResponseEntity<CreateGameworldResponseDto> create(@RequestBody CreateGameworldDto dto) {
    var response = createNewGameworld.create(dto);
    return ResponseEntity.ok().body(response);
  }
}
