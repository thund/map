package de.dungeonspace.map.application.event;

import de.dungeonspace.map.application.event.outgoing.FullPlanetDto;
import de.dungeonspace.map.application.event.outgoing.FullResourceDto;
import de.dungeonspace.map.application.event.outgoing.GameworldCreatedDto;
import de.dungeonspace.map.application.event.outgoing.PlanetNeighbourDto;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.primitive.Coordinates;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Component;

@Component
public class EventMapper {

  public GameworldCreatedDto map(Gameworld gameworld) {
    var planets = fromMapGrid(gameworld.getMapGrid());
    return new GameworldCreatedDto(gameworld.getId(), gameworld.getStatus(), planets);
  }

  private List<FullPlanetDto> fromMapGrid(MapGrid grid) {
    var planets = grid.getPlanets();
    return planets.entrySet().stream()
        .map(entry -> {
          var coords = entry.getKey();
          var planet = entry.getValue();
          var resource = planet.getResource();
          return new FullPlanetDto(
              planet.getId(),
              coords.getX(),
              coords.getY(),
              grid.areaOf(coords).getMovementDifficulty(),
              resource != null ? new FullResourceDto(
                  resource.getType(),
                  resource.getMaxAmount(),
                  resource.getCurrentAmount()
              ) : null
          );
        }).toList();
  }
}
