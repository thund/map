package de.dungeonspace.map.application.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.dungeonspace.map.application.event.incoming.RobotMovedIntegrationEvent;
import de.dungeonspace.map.application.event.incoming.RobotSpawnedIntegrationEvent;
import de.dungeonspace.map.application.event.outgoing.FullResourceDto;
import de.dungeonspace.map.application.event.outgoing.PlanetDiscoveredDto;
import de.dungeonspace.map.application.event.outgoing.PlanetNeighbourDto;
import de.dungeonspace.map.domain.gameworld.GameworldRepository;
import de.dungeonspace.map.domain.planet.PlanetRepository;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.stream.StreamSupport;
import lombok.AllArgsConstructor;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.Header;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@AllArgsConstructor
public class RobotIntegrationEventListener {
  private final ObjectMapper objectMapper;
  private final KafkaTemplate<String, String> kafkaTemplate;
  private final PlanetRepository planetRepository;
  private final GameworldRepository gameWorldRepository;

  @KafkaListener(topics = "robot.integration")
  @Transactional
  public void handleRobotMovedEvent(ConsumerRecord<String, String> event) throws Exception {
    var type = new String(event.headers().lastHeader("type").value());
    var payload = event.value();

    UUID planetId = null;
    switch (type) {
      case "RobotSpawnedIntegrationEvent" -> {
        var robotSpawnedEvent = objectMapper.readValue(payload, RobotSpawnedIntegrationEvent.class);
        planetId = robotSpawnedEvent.robot().planet().planetId();
      }
      case "RobotMovedIntegrationEvent" -> {
        var robotMovedEvent = objectMapper.readValue(payload, RobotMovedIntegrationEvent.class);
        planetId = robotMovedEvent.toPlanet().id();
      }
      default -> {
        return;
      }
    }

    var playerIdHeaders = StreamSupport.stream(event.headers().headers("playerId").spliterator(), false)
            .map(h -> new String(h.value()))
            .map(UUID::fromString)
            .toList();

   publishPlanetDiscoveredEvent(planetId, playerIdHeaders);
  }

  private void publishPlanetDiscoveredEvent(UUID planetId, Collection<UUID> playerIds) {
    var planet = planetRepository.findById(planetId).orElseThrow();
    var gw = gameWorldRepository.getGameworldOfPlanet(planet.getId());
    var grid = gw.getMapGrid();
    var coordinatesOfPlanet = grid.coordinatesOf(planet);
    var neighbours = grid.getDirectNeighbours(coordinatesOfPlanet);

    var neighbourDtos = neighbours.entrySet().stream()
        .map(entry -> new PlanetNeighbourDto(entry.getValue().getId(), coordinatesOfPlanet.getRelativeDirectionTo(entry.getKey())))
        .toList();
    var resource = planet.getResource();
    var planetDiscoveredEvent = new PlanetDiscoveredDto(
        planet.getId(),
        grid.areaOf(coordinatesOfPlanet).getMovementDifficulty(),
        neighbourDtos,
        resource != null ? new FullResourceDto(resource.getType(), resource.getMaxAmount(), resource.getCurrentAmount()) : null
    );

    String planetDiscoveredEventJson = null;
    try {
      planetDiscoveredEventJson = objectMapper.writeValueAsString(planetDiscoveredEvent);
    } catch (JsonProcessingException e) {
      throw new RuntimeException(e);
    }
    List<Header> headers = new ArrayList<>();
    playerIds.forEach(playerId -> headers.add(new RecordHeader("playerId", playerId.toString().getBytes())));
    headers.add(new RecordHeader("type", "PlanetDiscovered".getBytes()));
    headers.add(new RecordHeader("timestamp", Instant.now().toString().getBytes()));
    var record = new ProducerRecord<>("planet", null, planet.getId().toString(), planetDiscoveredEventJson, headers);
    kafkaTemplate.send(record);
  }
}
