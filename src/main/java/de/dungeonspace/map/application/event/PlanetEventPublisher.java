package de.dungeonspace.map.application.event;

import de.dungeonspace.map.domain.planet.event.ResourceMined;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class PlanetEventPublisher {
  private final EventPublisher eventPublisher;

  @EventListener
  public void handleResourceMinedEvent(ResourceMined event) {
    var message = MessageBuilder.withPayload(event)
        .setHeader("type", "ResourceMined")
        .setHeader(KafkaHeaders.TOPIC, "planet")
        .setHeader(KafkaHeaders.KEY, event.planet().toString())
        .build();
    eventPublisher.publish(message);
  }
}
