package de.dungeonspace.map.application.event.incoming;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.UUID;

@JsonIgnoreProperties(ignoreUnknown = true)
public record RobotMovedIntegrationEvent(
  RobotMovement toPlanet
) {
  public record RobotMovement(
      UUID id
  ) {}
}
