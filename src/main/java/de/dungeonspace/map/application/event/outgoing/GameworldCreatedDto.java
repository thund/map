package de.dungeonspace.map.application.event.outgoing;

import de.dungeonspace.map.domain.gameworld.GameworldStatus;
import java.util.List;
import java.util.UUID;

public record GameworldCreatedDto(
    UUID id,
    GameworldStatus status,
    List<FullPlanetDto> planets
) {

}
