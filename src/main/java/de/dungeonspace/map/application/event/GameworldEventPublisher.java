package de.dungeonspace.map.application.event;

import de.dungeonspace.map.domain.gameworld.events.GameworldCreated;
import de.dungeonspace.map.domain.gameworld.events.GameworldStatusChanged;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class GameworldEventPublisher {
  private final EventPublisher eventPublisher;
  private final EventMapper eventMapper;

  @EventListener
  public void handleGameworldCreatedEvent(GameworldCreated event) {
    var dto = eventMapper.map(event.gameworld());
    var message = MessageBuilder
        .withPayload(dto)
        .setHeader("type", "GameworldCreated")
        .setHeader(KafkaHeaders.TOPIC, "gameworld")
        .setHeader(KafkaHeaders.KEY, event.gameworld().getId().toString())
        .build();
    eventPublisher.publish(message);
  }

  @EventListener
  public void handleGameworldStatusEvent(GameworldStatusChanged event) {
    var message = MessageBuilder
        .withPayload(event)
        .setHeader("type", "GameworldStatusChanged")
        .setHeader(KafkaHeaders.TOPIC, "gameworld")
        .setHeader(KafkaHeaders.KEY, event.id().toString())
        .build();
    eventPublisher.publish(message);
  }
}
