package de.dungeonspace.map.application.event.outgoing;

import java.util.UUID;

public record FullPlanetDto(
    UUID id,
    int x,
    int y,
    int movementDifficulty,
    FullResourceDto resource
) {

}
