package de.dungeonspace.map.application.event;

import org.springframework.messaging.Message;

public interface EventPublisher {
    <T> void publish(Message<T> event);
}
