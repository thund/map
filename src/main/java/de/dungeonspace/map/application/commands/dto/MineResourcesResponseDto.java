package de.dungeonspace.map.application.commands.dto;

public record MineResourcesResponseDto(int amountMined) {

}
