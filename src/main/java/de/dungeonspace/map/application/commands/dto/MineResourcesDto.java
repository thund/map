package de.dungeonspace.map.application.commands.dto;

public record MineResourcesDto(int amountToMine) {

}
