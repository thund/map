package de.dungeonspace.map.application.commands.dto;

import java.util.UUID;

public record CreateGameworldResponseDto(UUID gameworldId) {}
