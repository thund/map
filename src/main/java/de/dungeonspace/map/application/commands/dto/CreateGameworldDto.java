package de.dungeonspace.map.application.commands.dto;

public record CreateGameworldDto(int playerAmount) {}
