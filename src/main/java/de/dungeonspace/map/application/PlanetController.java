package de.dungeonspace.map.application;

import de.dungeonspace.map.application.commands.CreateNewGameworld;
import de.dungeonspace.map.application.commands.MineResources;
import de.dungeonspace.map.application.commands.dto.CreateGameworldDto;
import de.dungeonspace.map.application.commands.dto.MineResourcesDto;
import de.dungeonspace.map.application.commands.dto.MineResourcesResponseDto;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/planets")
@RequiredArgsConstructor
public class PlanetController {
  private final MineResources mineResources;

  @PostMapping(value = "{id}/minings", produces = "application/json", consumes = "application/json")
  public ResponseEntity<MineResourcesResponseDto> create(@PathVariable("id") UUID id, @RequestBody MineResourcesDto dto) {
    var response = mineResources.mine(id, dto);
    return ResponseEntity.ok(response);
  }
}
