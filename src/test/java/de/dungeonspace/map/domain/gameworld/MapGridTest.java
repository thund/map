package de.dungeonspace.map.domain.gameworld;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.primitive.Coordinates;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;

class MapGridTest {

  @Test
  void shouldCreateMapGridWithSize() {
    var mapGrid = MapGrid.create(10);
    assertEquals(10 * 10, mapGrid.getPlanets().size());
  }

  @Test
  void shouldDestroyPlanet() {
    var mapGrid = MapGrid.create(10);
    var coordinates = new Coordinates(0, 0);
    assertTrue(mapGrid.getPlanets().containsKey(coordinates));

    mapGrid.destroyPlanet(coordinates);

    assertFalse(mapGrid.getPlanets().containsKey(coordinates));
  }

  @Test
  void shouldNotCreateNewPlanet() {
    var mapGrid = MapGrid.create(10);
    var coordinates = new Coordinates(100, 100);
    assertFalse(mapGrid.getPlanets().containsKey(coordinates));

    var patch = Map.of(coordinates, Planet.create());
    mapGrid.patchPlanets(patch);
    assertFalse(mapGrid.getPlanets().containsKey(coordinates));
  }

  @Test
  void shouldPatchPlanet() {
    var mapGrid = MapGrid.create(10);
    var coordinates = new Coordinates(1, 1);
    var newPlanet = Planet.create();
    assertFalse(mapGrid.getPlanets().get(coordinates).equals(newPlanet));

    var patch = Map.of(coordinates, newPlanet);
    mapGrid.patchPlanets(patch);
    assertTrue(mapGrid.getPlanets().get(coordinates).equals(newPlanet));
  }

  @ParameterizedTest
  @CsvSource(value = {"0,0,BORDER", "1,1,OUTER", "2,2,MID", "5,5,INNER"})
  void shouldReturnArea(int x, int y, Area area) {
    var mapGrid = MapGrid.create(10);
    var coords = mapGrid.areaOf(new Coordinates(x, y));
    assertEquals(area, coords);
  }

  @Test
  void shouldReturnDistinctArea() {
    var mapGrid = MapGrid.create(10);
    var areas = Arrays.stream(Area.values()).map(mapGrid::getPlanets).toList();
    assertThat(areas.size())
        .isEqualTo(Area.values().length);
    assertThat(areas)
        .noneMatch(Map::isEmpty);

    for(var area : areas) {
      var withoutArea = areas.stream().filter(a -> !a.equals(area)).toList();
      assertThat(withoutArea.stream().flatMap(m -> m.keySet().stream()))
          .doesNotContainAnyElementsOf(area.keySet());
    }
  }

  @Test
  void shouldReturnDirectNeighbours() {
    var mapGrid = MapGrid.create(10);
    var coordinates = new Coordinates(1, 1);
    var neighbours = mapGrid.getDirectNeighbours(coordinates);

    assertThat(neighbours)
        .containsOnlyKeys(
            new Coordinates(0, 1),
            new Coordinates(2, 1),
            new Coordinates(1, 0),
            new Coordinates(1, 2)
        );
  }
}
