package de.dungeonspace.map.domain.gameworld.factory;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

import de.dungeonspace.map.domain.planet.Planet;
import org.junit.jupiter.api.Test;

class DefaultGameworldFactoryTest {

  @Test
  void shouldCreateGameworld() {
    // Hard to test, just check if everything looks fine
    var factory = new DefaultGameworldFactory(10);
    var gameworld = factory.create();
    assertNotNull(factory.create());

    var grid = gameworld.getMapGrid();
    // Check that planets have been deleted
    assertThat(grid.getPlanets())
        .hasSizeLessThan(grid.getSize() * grid.getSize());
    // Check that the majority of planets have not been deleted
    assertThat(grid.getPlanets())
        .hasSizeGreaterThan((grid.getSize() * grid.getSize()) / 2);
    
    // Check that there are resources
    var planets = grid.getPlanets().values();
    assertThat(planets)
        .filteredOn(Planet::hasResource)
        .hasSizeGreaterThan(0);
  }
}
