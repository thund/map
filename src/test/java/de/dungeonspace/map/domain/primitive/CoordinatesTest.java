package de.dungeonspace.map.domain.primitive;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class CoordinatesTest {
  @ParameterizedTest
  @CsvSource(value = { "1,0", "0,1", "1,2", "2,1" })
  void shouldReturnTrueWhenAdjacent(int x, int y) {
    var coordinates = new Coordinates(1, 1);
    var otherCoordinates = new Coordinates(x, y);
    assertTrue(coordinates.isAdjacent(otherCoordinates));
  }

  @ParameterizedTest
  @CsvSource(value = { "1,1", "0,0", "2,2", "2,0", "0,2" })
  void shouldReturnFalseWhenNotAdjacent(int x, int y) {
    var coordinates = new Coordinates(1, 1);
    var otherCoordinates = new Coordinates(x, y);
    assertFalse(coordinates.isAdjacent(otherCoordinates));
  }

  @ParameterizedTest
  @CsvSource(value = { "1,0,SOUTH", "0,1,WEST", "1,2,NORTH", "2,1,EAST" })
  void shouldReturnDirection(int x, int y, Direction direction) {
    var coordinates = new Coordinates(1, 1);
    var otherCoordinates = new Coordinates(x, y);
    assertEquals(direction, coordinates.getRelativeDirectionTo(otherCoordinates));
  }
}
