package de.dungeonspace.map.domain.planet;

import static org.junit.jupiter.api.Assertions.*;

import de.dungeonspace.map.domain.planet.exception.NoResourceAvailableException;
import org.junit.jupiter.api.Test;

class PlanetTest {

  @Test
  void shouldReturnFalseWhenNoResource() {
    var planet = new Planet();
    assertFalse(planet.hasResource());
  }

  @Test
  void shouldReturnTrueWhenResource() {
    var planet = new Planet();
    planet.createResource(new Resource(ResourceType.IRON, 100));
    assertTrue(planet.hasResource());
  }

  @Test
  void shouldThrowWhenPlanetHasNoResource() {
    var planet = new Planet();
    assertThrows(NoResourceAvailableException.class, () -> planet.mine(100));
  }

  @Test
  void shouldThrowWhenPlanetHasAlreadyResource() {
    var planet = new Planet();
    planet.createResource(new Resource(ResourceType.IRON, 100));
    assertThrows(RuntimeException.class, () -> planet.createResource(new Resource(ResourceType.IRON, 100)));
  }

  @Test
  void shouldMineAmount() {
    var planet = new Planet();
    var resource = new Resource(ResourceType.IRON, 100);
    planet.createResource(resource);
    planet.mine(50);
    assertEquals(50, planet.getResource().getCurrentAmount());
  }

  @Test
  void shouldMineDiff() {
    var planet = new Planet();
    var resource = new Resource(ResourceType.IRON, 40);
    planet.createResource(resource);
    planet.mine(50);
    assertEquals(0, planet.getResource().getCurrentAmount());
  }

  @Test
  void shouldThrowWhenNoResourceLeft() {
    var planet = new Planet();
    var resource = new Resource(ResourceType.IRON, 0);
    planet.createResource(resource);
    assertThrows(NoResourceAvailableException.class, () -> planet.mine(50));
  }

}
