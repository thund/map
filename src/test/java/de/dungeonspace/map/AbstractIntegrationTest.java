package de.dungeonspace.map;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.redpanda.RedpandaContainer;

@SpringBootTest
@ActiveProfiles("test")
public class AbstractIntegrationTest {
  static PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:alpine")
      .withDatabaseName("map")
      .withUsername("map")
      .withPassword("map");
  static RedpandaContainer redpanda = new RedpandaContainer("docker.redpanda.com/vectorized/redpanda:latest");

  static {
    postgres.start();
    redpanda.start();
  }

  @DynamicPropertySource
  static void setDatabase(DynamicPropertyRegistry registry) {
    registry.add("spring.datasource.url", postgres::getJdbcUrl);
    registry.add("spring.datasource.username", postgres::getUsername);
    registry.add("spring.datasource.password", postgres::getPassword);
  }

  @DynamicPropertySource
  static void setKafka(DynamicPropertyRegistry registry) {
    registry.add("spring.kafka.bootstrap-servers", redpanda::getBootstrapServers);
  }
}
