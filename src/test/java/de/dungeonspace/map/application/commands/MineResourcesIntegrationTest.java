package de.dungeonspace.map.application.commands;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.application.commands.dto.MineResourcesDto;
import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.planet.PlanetRepository;
import de.dungeonspace.map.domain.planet.Resource;
import de.dungeonspace.map.domain.planet.ResourceType;
import de.dungeonspace.map.domain.planet.event.ResourceMined;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.transaction.annotation.Transactional;

@RecordApplicationEvents
@Transactional
class MineResourcesIntegrationTest extends AbstractIntegrationTest {
  @Autowired
  private MineResources mineResources;

  @Autowired
  PlanetRepository planetRepository;

  @Autowired
  ApplicationEvents applicationEvents;

  @Test
  void shouldMineResources() {
    var planet = Planet.create();
    planet.createResource(new Resource(ResourceType.IRON, 100));

    planetRepository.save(planet);

    mineResources.mine(planet.getId(), new MineResourcesDto(10));
    assertThat(planetRepository.findById(planet.getId()).get().getResource().getCurrentAmount())
        .isEqualTo(90);
    assertThat(applicationEvents.stream(ResourceMined.class))
        .hasSize(1);
  }
}
