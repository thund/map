package de.dungeonspace.map.application.commands;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.*;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.application.commands.dto.CreateGameworldDto;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.GameworldRepository;
import de.dungeonspace.map.domain.gameworld.GameworldStatus;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.gameworld.events.GameworldCreated;
import de.dungeonspace.map.domain.gameworld.events.GameworldStatusChanged;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.transaction.annotation.Transactional;

@RecordApplicationEvents
@Transactional
class CreateNewGameworldIntegrationTest extends AbstractIntegrationTest {
  @Autowired
  private CreateNewGameworld createNewGameworld;

  @Autowired
  GameworldRepository gameworldRepository;

  @Autowired
  ApplicationEvents applicationEvents;

  @BeforeEach
  void setUp() {
    gameworldRepository.deleteAll();
  }

  @AfterEach
  void tearDown() {
    gameworldRepository.deleteAll();
  }

  @Test
  void shouldEndActiveGameworlds() {
    var anActiveGameworld = Gameworld.create(MapGrid.create(5));
    anActiveGameworld.changeStatus(GameworldStatus.ACTIVE);
    gameworldRepository.save(anActiveGameworld);

    createNewGameworld.create(new CreateGameworldDto(2));

    var gameworlds = gameworldRepository.findAll();
    assertEquals(2, gameworlds.size());
    var inActiveGameworld = gameworlds.stream()
        .filter(g -> g.getStatus() == GameworldStatus.INACTIVE)
        .findFirst()
        .orElseThrow();
    assertThat(inActiveGameworld.getId())
        .isEqualTo(anActiveGameworld.getId());
    assertThat(applicationEvents.stream(GameworldStatusChanged.class))
        .hasSizeGreaterThan(1);
  }

  @Test
  void newGameworldShouldBeActive() {
    createNewGameworld.create(new CreateGameworldDto(2));

    var gameworlds = gameworldRepository.findAll();
    assertEquals(1, gameworlds.size());
    assertThat(gameworlds.get(0).getStatus())
        .isEqualTo(GameworldStatus.ACTIVE);
  }

  @Test
  void shouldPublishGameworldCreatedEvent() {
    createNewGameworld.create(new CreateGameworldDto(2));
    assertThat(applicationEvents.stream(GameworldCreated.class))
        .hasSize(1);
  }
}
