package de.dungeonspace.map.application.event;

import static org.assertj.core.api.Assertions.*;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.domain.gameworld.Gameworld;
import de.dungeonspace.map.domain.gameworld.GameworldRepository;
import de.dungeonspace.map.domain.gameworld.GameworldStatus;
import de.dungeonspace.map.domain.gameworld.MapGrid;
import de.dungeonspace.map.domain.primitive.Coordinates;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.header.internals.RecordHeader;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.test.annotation.DirtiesContext;

@DirtiesContext
@TestInstance(Lifecycle.PER_CLASS)
class RobotIntegrationEventListenerIntegrationTest extends AbstractIntegrationTest {
  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  @Autowired
  private GameworldRepository gameworldRepository;

  @Autowired
  DefaultKafkaConsumerFactory consumerFactory;

  private BlockingQueue<ConsumerRecord<String, String>> consumerRecords;
  private KafkaMessageListenerContainer<String, String> container;

  @BeforeAll
  void setUp() {
    ContainerProperties containerProperties = new ContainerProperties("planet");
    container = new KafkaMessageListenerContainer<>(consumerFactory, containerProperties);
    consumerRecords = new LinkedBlockingQueue<>();
    container.setupMessageListener((MessageListener<String, String>) consumerRecords::add);
    container.start();
    ContainerTestUtils.waitForAssignment(container, 1);
  }

  @AfterAll
  void tearDown() {
    container.stop();
    gameworldRepository.deleteAll();
  }

  @Test
  void shouldPublishOnMove() throws InterruptedException {
    var gameworld = Gameworld.create(MapGrid.create(10));
    gameworld.changeStatus(GameworldStatus.ACTIVE);
    gameworldRepository.save(gameworld);
    var planet = gameworld.getMapGrid().getPlanets().get(new Coordinates(0, 0));

    var event = """
        {
          "toPlanet": {
            "id": "%s"
          }
        }
        """.formatted(planet.getId());
    var player1 = UUID.randomUUID().toString();
    var player2 = UUID.randomUUID().toString();
    var producerRecord = new ProducerRecord<String, String>("robot.integration", 0, null, event,
        List.of(
            new RecordHeader("type", "RobotMovedIntegrationEvent".getBytes()),
            new RecordHeader("playerId", player1.toString().getBytes()),
            new RecordHeader("playerId", player2.toString().getBytes())
        ));
    kafkaTemplate.send(producerRecord);

    var consumerRecord = consumerRecords.poll(10, TimeUnit.SECONDS);
    assertThat(consumerRecord)
        .isNotNull()
        .satisfies(record -> {
          assertThat(record.key()).isEqualTo(planet.getId().toString());
          assertThat(record.headers().lastHeader("type").value()).isEqualTo("PlanetDiscovered".getBytes());
          assertThat(record.headers().headers("playerId"))
              .hasSize(2)
              .map(header -> new String(header.value()))
              .containsExactly(player1.toString(), player2.toString());
        });
  }

  @Test
  void shouldPublishOnSpawn() throws InterruptedException {
    var gameworld = Gameworld.create(MapGrid.create(10));
    gameworld.changeStatus(GameworldStatus.ACTIVE);
    gameworldRepository.save(gameworld);
    var planet = gameworld.getMapGrid().getPlanets().get(new Coordinates(0, 0));

    var event = """
        {
          "robot": {
            "planet": {
              "planetId": "%s"
            }
          }
        }
        """.formatted(planet.getId());
    var player1 = UUID.randomUUID().toString();
    var producerRecord = new ProducerRecord<String, String>("robot.integration", 0, null, event,
        List.of(
            new RecordHeader("type", "RobotSpawnedIntegrationEvent".getBytes()),
            new RecordHeader("playerId", player1.toString().getBytes())
        ));
    kafkaTemplate.send(producerRecord);

    var consumerRecord = consumerRecords.poll(10, TimeUnit.SECONDS);
    assertThat(consumerRecord)
        .isNotNull()
        .satisfies(record -> {
          assertThat(record.key()).isEqualTo(planet.getId().toString());
          assertThat(record.headers().lastHeader("type").value()).isEqualTo("PlanetDiscovered".getBytes());
          assertThat(record.headers().headers("playerId"))
              .hasSize(1)
              .map(header -> new String(header.value()))
              .containsExactly(player1.toString());
        });
  }
}
