package de.dungeonspace.map.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.domain.planet.Planet;
import de.dungeonspace.map.domain.planet.PlanetRepository;
import de.dungeonspace.map.domain.planet.Resource;
import de.dungeonspace.map.domain.planet.ResourceType;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
public class PlanetControllerIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  PlanetRepository planetRepository;

  @Test
  void shouldMineResources() throws Exception {
    var planet = Planet.create();
    planet.createResource(new Resource(ResourceType.IRON, 100));
    planetRepository.save(planet);

    mockMvc.perform(
            post("/planets/{id}/minings", planet.getId())
                .accept("application/json")
                .contentType("application/json")
                .content("{\"amountToMine\": 50}")
        )
        .andDo(print())
        .andExpect(status().isOk());

    assertThat(planetRepository.findById(planet.getId()).get().getResource().getCurrentAmount())
        .isEqualTo(50);
  }

  @Test
  void shouldReturnBadRequestOnNoResource() throws Exception {
    var planet = Planet.create();
    planetRepository.save(planet);

    mockMvc.perform(
            post("/planets/{id}/minings", planet.getId())
                .accept("application/json")
                .contentType("application/json")
                .content("{\"amountToMine\": 50}")
        )
        .andDo(print())
        .andExpect(status().isBadRequest());
  }

  @Test
  void shouldReturnNotFoundOnUnknownPlanet() throws Exception {
    mockMvc.perform(
            post("/planets/{id}/minings", UUID.randomUUID())
                .accept("application/json")
                .contentType("application/json")
                .content("{\"amountToMine\": 50}")
        )
        .andDo(print())
        .andExpect(status().isNotFound());
  }
}
