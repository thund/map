package de.dungeonspace.map.application;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import de.dungeonspace.map.AbstractIntegrationTest;
import de.dungeonspace.map.domain.gameworld.GameworldRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
public class GameworldControllerIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  MockMvc mockMvc;

  @Autowired
  GameworldRepository gameworldRepository;

  @Test
  void shouldCreateNewGameworld() throws Exception {
    mockMvc.perform(
            post("/gameworlds")
                .accept("application/json")
                .contentType("application/json")
                .content("{\"playerAmount\": 2}")
        )
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.gameworldId", notNullValue()));

    assertThat(gameworldRepository.findAll())
        .hasSize(1);
  }
}
