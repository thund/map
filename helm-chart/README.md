# map

This project contains the deployment for https://gitlab.com/the-microservice-dungeon/map

# Installation

To install this to your Minikube K8s Cluster run:
```shell
$ helm install map . -n map --create-namespace
```

If your run
```shell
$ minikube tunnel
```
the service is exposed to port `30003`

### Design
We try to make our Helm Chart configurable, as much as possible, via the values.yaml
so you don't have to adjust the Helm Chart everytime you want to make a change.
