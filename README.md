# Map Core Service

## Prerequisites

- [Docker](https://www.docker.com/)
- [Local-Dev-Environment](https://gitlab.com/the-microservice-dungeon/local-dev-environment)

## Start

1. Start the local dev environment according to the documentation

2. Start the map core service

```java
./mvnw spring-boot:run
```

## APIs

Documentation of the APIs can be found as [OpenAPI](./docs/openapi.yaml) and [AsyncAPI](./docs/asyncapi.yaml).
